﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluación.Models
{
    //PRIMERO SE CREA EL MODELO DE USUARIOS CON SU ID NOMBRE APELLIDO CORREO Y CON SUS GET Y SET 
    public class UsersModel
    {
        //GETTER Y SETTER

        public int Id { get; set; }
        public string Name { get; set; }
        public string Correo { get; set; }
        public string Password { get; set; }

        //METÓDO VACÍO
        public UsersModel() { }

        //MÉTODO LLENO con los tipos de valores y los valores que se va recibir para declararlos
        public UsersModel(int id, string name, string correo, string password) { Id = id; Name = name;  Correo = correo; Password = password;  }

        //TOSTRING para imprimir la información cuando se desee mostrar
        public override string ToString()
        {
            return ("Nombre: " + Name + "id: " + Id + "Correo: "+Correo );
        }
    }

}

