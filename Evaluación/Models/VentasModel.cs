﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluación.Models
{
    //LUEGO SE CREA EL MODELO DE VENTAS 
    public class VentasModel
    {
        //GETTER Y SETTER
        public int Id{ get; set; }
        public string Producto { get; set; }
        public int Cantidad { get; set; }
        public int Valor{ get; set;}

        //MÉTODO VACÍO
        public VentasModel() { }

        //MÉTODO LLENO
        public VentasModel(int id, string producto, int cantidad, int valor){ Id = id; Producto = producto; Cantidad = cantidad; Valor = valor; }
        //TOSTRING
        public override string ToString()
        {
            return ("Nombre del producto: "+Producto+" Cantidad del producto: "+Cantidad+" Valor: "+Valor);
        }
    }
}
