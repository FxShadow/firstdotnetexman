﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluación.Models
{
    //LUEGO SE CREA EL MODELO DE CLIENTS MODEL QUE VA A HEREDAR DE USERSMODEL TODA LA INFORMACIÓN EXCEPTO EL TELEFONO
    public class ClientsModel: UsersModel
    {
        //GETTER Y SETTER
        public string Telefono { get; set; }
        //MÉTODO VACÍO
        public ClientsModel() { }

        //MÉTODO LLENO
        public ClientsModel(int id, string name, string correo,string telefono) { Id = id; Name = name; Correo = correo; Telefono = telefono; }

        //TOSTRING
        public override string ToString()
        {
            return (" Id: " + Id + " Nombre: " +Name+" Correo: "+Correo+" Telefono: "+Telefono);
        }
    }
}
