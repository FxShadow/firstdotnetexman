﻿using Evaluación.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluación.Services
{
    public class VentasServices
    {
        //LISTA

        public List<VentasModel> listVentas = new List<VentasModel>();

        //MENÚ DE VENTAS
     
        public void NavbarVentas()
        {
            int opcion = 0;
            do
            {
                Console.WriteLine("Que deseas hacer hoy: 1. Registrar 2.Listar 3.Eliminar 4. Buscar 5.Reporte de ventas  6. Salir");
                opcion = int.Parse(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        Console.WriteLine("Bienvenido a  Registrar");
                        RegistrarVentas();
                        break;
                    case 2:
                        Console.WriteLine("Bienvenido a Listar");
                        ListarVentas();
                        break;
                    case 3:
                        Console.WriteLine("Bienvenido a Eliminar");
                        EliminarVentas();
                        break;
                    case 4:
                        Console.WriteLine("Bienvenido a buscar");
                        BuscarVentas();
                        break;
                    case 5:
                        Console.WriteLine("Bienvenido a Reporte de ventas");
                        ReporteVentas();
                        break;
                    case 6:
                        Console.WriteLine("Gracias");
                        break;
                }
            }
            while (opcion != 6);
        }
        //REGISTRO

        public void RegistrarVentas()
        {
            Console.WriteLine("Ingrese el id: ");
            int id = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese el nombre: ");
            string nombre = Console.ReadLine();
            Console.WriteLine("Ingrese la cantidad: ");
            int cantidad= int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese el valor: ");
            int valor = int.Parse(Console.ReadLine());

            listVentas.Add(new VentasModel(id, nombre, cantidad, valor));
        }
        //LISTAR
        public void ListarVentas()
        {

            foreach (var C in listVentas)
            {
                if (listVentas.Count == 0)
                {
                    Console.WriteLine("Sin ventas registradas");
                }
                else
                {
                    Console.WriteLine(C.ToString());
                }

            }
        }

        public void BuscarVentas()
        {
            Console.WriteLine("Ingrese el id de la venta: ");
            int id = int.Parse(Console.ReadLine());
            foreach (var C in listVentas)
            {
                if (id == C.Id)
                {
                    Console.WriteLine(C.ToString());
                }
                else
                {
                    Console.WriteLine("Venta no encontrada");
                }
            }
        }

        public void EliminarVentas()
        {
            Console.WriteLine("Ingrese el id de la venta que desea eliminar: ");
            int id = int.Parse(Console.ReadLine());
            foreach (var C in listVentas)
            {
                if (id == C.Id)
                {
                    listVentas.Remove(C);
                    Console.WriteLine("eliminado exitosamente");
                    break;
                }
                else
                {
                    Console.WriteLine("Venta no encontrada");
                }
            }
        }
        public void ReporteVentas()
        {

            foreach (var C in listVentas)
            {
                if (listVentas.Count == 0)
                {
                    Console.WriteLine("Sin ventas registradas");
                }
                else
                {
                    double suma = listVentas.Sum(item => item.Valor*item.Cantidad);
                    Console.WriteLine(C.ToString());
                    Console.WriteLine("VENTA TOTAL: " + suma);
                }

            }
        }

        public void Assets()
        {
            listVentas.Add(new VentasModel(02, "LECHE",12, 1000));
     
        }
    }
}
