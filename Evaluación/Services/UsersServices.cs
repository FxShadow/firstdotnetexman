﻿using Evaluación.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluación.Services
{
    public class UsersServices
    {
        public List<UsersModel> listUsers = new List<UsersModel>();
        public VentasServices ventas = new VentasServices();
        public ClientsServices client = new ClientsServices();

        //CUANDO EL USUARIO INGRESA EL LOGIN LE APARECERA ESTE NAVBAR DONDE SE PODRA DIRIGIR AL CRUD DE USUARIOS, AL CRUD DE CLIENTES Y AL REPORTE DE VENTAS
        public void Navbar()
        {
            int opcion = 0;
            do
            {
                Console.WriteLine("Que deseas hacer hoy: 1. Ventas 2.Usuarios 3.Clientes");
                opcion = int.Parse(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        Console.WriteLine("Bienvenido a  Ventas");
                        ventas.NavbarVentas();
                        break;
                    case 2:
                        Console.WriteLine("Bienvenido a Usuarios");
                        NavbarUsuarios();
                        break;
                    case 3:
                        Console.WriteLine("Bienvenido a Clientes");
                        client.NavbarClientes();
                        break;
                }
            }
            while (opcion != 3);
        }
        //ESTE ES EL MENÚ DE USUARIOS
        public void NavbarUsuarios()
        {
            int opcion = 0;
            do
            {
                Console.WriteLine("Que deseas hacer hoy: 1. Registrar 2.Listar 3.Eliminar 4. Buscar 5. editar 6.salir");
                opcion = int.Parse(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        Console.WriteLine("Bienvenido a  Registrar");
                        RegistrarUser();
                        break;
                    case 2:
                        Console.WriteLine("Bienvenido a Listar");
                        ListarUser();
                        break;
                    case 3:
                        Console.WriteLine("Bienvenido a Eliminar");
                        EliminarUser();
                        break;
                    case 4:
                        Console.WriteLine("Bienvenido a buscar");
                        BuscarUser();
                        break;
                    case 5:
                        Console.WriteLine("Bienvenido a editar");
                        EditarUser();
                        break;
                }
            }
            while (opcion != 6);
        }
        //REGISTRO

        public void RegistrarUser()
        {
            int opcion = 0;
            
                Console.WriteLine("Desea 1. Registrar automaticamente 2. Registrar manualmente");
                opcion = int.Parse(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        Assets();
                        break;
                    case 2:
                        Console.WriteLine("Ingrese su cedula: ");
                        int id = int.Parse(Console.ReadLine());
                        Console.WriteLine("Ingrese el nombre: ");
                        string nombre = Console.ReadLine();
                        Console.WriteLine("Ingrese el Correo: ");
                        string correo = Console.ReadLine();
                        Console.WriteLine("Ingrese la contraseña: ");
                        string password = Console.ReadLine();

                        listUsers.Add(new UsersModel(id, nombre, correo, password));
                        break;
                    default:
                        break;
                }

        }
        //LISTAR
        public void ListarUser()
        {

            foreach (var C in listUsers)
            {
                if (listUsers.Count == 0)
                {
                    Console.WriteLine("Usuario no encontrado");
                }
                else
                {
                    Console.WriteLine(C.ToString());
                }

            }
        }

        public void BuscarUser()
        {
            var C = BuscadorUser();
            if (C != null)
            {
                Console.WriteLine(C.ToString());

            }
            else
            {
                Console.WriteLine("Usere no encontrado");
            }
        }
        public UsersModel BuscadorUser()
        {
            Console.Write("Ingresar documento: ");
            int doc = int.Parse(Console.ReadLine());

            if (listUsers.Count > 0)
            {
                foreach (var i in listUsers)
                {
                    if (i.Id == doc)
                    {
                        return i;
                    }
                }
            }
            return null;
        }

        public void EditarUser()
        {
            var c = BuscadorUser();
            if (c != null)
            {
                Console.WriteLine("Que deseas editar 1. nombre 2.correo 3. contraseña 4.salir");
                int opcion = int.Parse(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        Console.WriteLine("Ingrese el nuevo nombre");
                        string nombre = Console.ReadLine();
                        c.Name = nombre;
                        Console.WriteLine("Editado correctamente");
                        break;
                    case 2:
                        Console.WriteLine("Ingrese el nuevo correo");
                        string correo = Console.ReadLine();
                        c.Correo = correo;
                        Console.WriteLine("Editado correctamente");
                        break;
                    case 3:
                        Console.WriteLine("Ingrese la nueva contraseña");
                        string Password = Console.ReadLine();
                        c.Password = Password;
                        Console.WriteLine("Editado correctamente");
                        break;
                    case 4:
                        break;
                    default:
                        Console.WriteLine("Opcion no disponible");
                        break;
                }
            }
            else
            {
                Console.WriteLine("Usuario no encontrado");
            }
        }
        public void EliminarUser()
        {
            var C = BuscadorUser();
            if (C != null)
            {
                listUsers.Remove(C);

            }
            else
            {
                Console.WriteLine("Usere no encontrado");
            }

        }

        public void Assets()
        {
            listUsers.Add(new UsersModel(1001361171, "KATHERIN", "correo", "123"));
            listUsers.Add(new UsersModel(01, "DANIEL", "correo", "121213"));
            listUsers.Add(new UsersModel(01, "MANUEL", "correo1212", "12"));
            Console.WriteLine("Registrado exitosamente");
        }
        //ETC Y SEEDERS

    }
}

