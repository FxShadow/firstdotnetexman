﻿using Evaluación.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluación.Services
{
    public class ClientsServices
    {
        //LISTA

        
        public List<ClientsModel> listClients = new List<ClientsModel> ();

        VentasServices ventas = new VentasServices ();

        //MENÚ DE CLIENTES
        
        public void NavbarClientes()
        {
            int opcion = 0;
            do
            {
                Console.WriteLine("Que deseas hacer hoy: 1. Registrar 2.Listar 3.Eliminar 4. Buscar 5. editar 6.salir");
                opcion = int.Parse(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        Console.WriteLine("Bienvenido a  Registrar");
                        RegistrarClient();
                        break;
                    case 2:
                        Console.WriteLine("Bienvenido a Listar");
                        ListarClient();
                        break;
                    case 3:
                        Console.WriteLine("Bienvenido a Eliminar");
                        EliminarClient();
                        break;
                    case 4:
                        Console.WriteLine("Bienvenido a buscar");
                        BuscarClient();
                        break;
                    case 5:
                        Console.WriteLine("Bienvenido a editar");
                        EditarClient();
                        break;
                }
            }
            while (opcion != 6);
        }
        //REGISTRO

        public void RegistrarClient()
        {
            int opcion = 0;
            do
            {
                Console.WriteLine("Desea 1. Registrar automaticamente 2. Registrar manualmente 3.volver");
                opcion = int.Parse(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        Assets();
                        break;
                    case 2:
                        Console.WriteLine("Ingrese su cedula: ");
                        int id = int.Parse(Console.ReadLine());
                        Console.WriteLine("Ingrese el nombre: ");
                        string nombre = Console.ReadLine();
                        Console.WriteLine("Ingrese el Correo: ");
                        string correo = Console.ReadLine();
                        Console.WriteLine("Ingrese Telefono: ");
                        string telefono = Console.ReadLine();

                        listClients.Add(new ClientsModel(id, nombre, correo, telefono));
                        break;
                    case 3:
                        break;
                    default:
                        break;
                }
                
            } while (opcion!=3);
          
        }
        //LISTAR
        public void ListarClient()
        {

            foreach(var C in listClients)
            {
                if(listClients.Count== 0) {
                    Console.WriteLine("Cliente no encontrado");
                }
                else
                {
                    Console.WriteLine(C.ToString());
                }
           
            }
        }

        public void BuscarClient()
        {
            var C = BuscadorClient();
            if (C != null)
            {
                Console.WriteLine(C.ToString());

            }
            else
            {
                Console.WriteLine("Cliente no encontrado");
            }
        }
        public ClientsModel BuscadorClient()
        {
            Console.Write("Ingresar documento: ");
            int doc = int.Parse(Console.ReadLine());

            if (listClients.Count > 0)
            {
                foreach (var i in listClients)
                {
                    if (i.Id == doc)
                    {
                        return i;
                    }
                }
            }
            return null;
        }

        public void EditarClient() {
            var c = BuscadorClient();
            if (c != null)
            {
                Console.WriteLine("Que deseas editar 1. nombre 2.correo 3. telefono 4.salir" );
                int opcion = int.Parse(Console.ReadLine());
                switch (opcion) { 
                case 1:
                        Console.WriteLine("Ingrese el nuevo nombre");
                        string nombre = Console.ReadLine();
                        c.Name= nombre;
                        Console.WriteLine("Editado correctamente");
                        break;
                    case 2:
                        Console.WriteLine("Ingrese el nuevo correo");
                        string correo = Console.ReadLine();
                        c.Correo = correo;
                        Console.WriteLine("Editado correctamente");
                        break;
                    case 3:
                        Console.WriteLine("Ingrese la nueva contraseña");
                        string Telefono = Console.ReadLine();
                        c.Telefono = Telefono;
                        Console.WriteLine("Editado correctamente");
                        break;
                    case 4:
                        break;
                    default: Console.WriteLine("Opcion no disponible");
                        break;
                }
        }
            else {
                Console.WriteLine("Cliente no encontrado");
                    }
         }
        public void EliminarClient()
        {
            var C = BuscadorClient();
            if (C != null)
            {
                listClients.Remove(C);

            }
            else
            {
                Console.WriteLine("Cliente no encontrado");
            }
            
            }

        public void Assets()
        {
            listClients.Add(new ClientsModel(1001361171, "KATHERIN", "correo", "123"));
            listClients.Add(new ClientsModel(01, "DANIEL", "correo", "121213"));
            listClients.Add(new ClientsModel(01, "MANUEL", "correo1212", "12"));
            Console.WriteLine("Registrado exitosamente");
        }
        //ETC Y SEEDERS

    }
}
