﻿using Evaluación.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Evaluación
{
    internal class Program
    {
        //EN PROGRAM SE CREA EL MENU CON EL LOGIN Y EL SIGN UP
        static void Main(string[] args)
        {
            //SE INSTANCIA LA CLASE LLAMADA LOGIN CON EL NOMBRE DE LOGIN PARA PODER ACCEDER A SUS PROPIOS METODOS
            Login login = new Login();
            //SE INSTANCIA LA CLASE LLAMADA CLIENTSERVICES CON EL NOMBRE DE CLIENT PARA PODER ACCEDER A SUS PROPIOS METODOS
            ClientsServices cliente = new ClientsServices();
            //SE INSTANCIA LA CLASE LLAMADA CLIENTSERVICES CON EL NOMBRE DE CLIENT PARA PODER ACCEDER A SUS PROPIOS METODOS
            UsersServices user = new UsersServices();
            //SE INICIA EL MENÚ
            int opcion = 0;
            ///SE CREA UN DO WHILE CON LA CONDICION MIENTRAS SEA DISTINTO A 3 PODRA SEGUIR EJECUTANDOSE
            do
            {
                //SE DA LAS OPCIONES DEL MENÚ 
                Console.WriteLine("Desea 1. Ingresar 2. Registrarse 3. salir");
                //SE GUARDAN LA OPCION ELEGIDA Y SE LE HACE UN PARSE PARA QUE PUEDA GUARDARSE COMO INT
                int menu = int.Parse(Console.ReadLine());

                //SE HACE UN SWITCH CON LA OPCION ELEGIDA EN EL CUAL EL 1. SERA IR AL LOGIN, EL 2. SERA IR AL REGISTRO, Y EL 3 PARA SALIR
                switch (menu)
                {
                    case 1: 
                        Console.WriteLine("Bienvenido a Login");
                        //Se llama el metodo
                        login.LoginU();
                        break;
                    case 2:
                        Console.WriteLine("Bievenido al registro");
                        //Se llama el metodo DE REGISTRAR UN NUEVO USUARIO QUE LO TIENE LA CLASE USUARIO
                        login.Registro();
                        break;
                    case 3:
                        Console.WriteLine("Gracias por participar");
                        opcion = 3;
                        break;
                    default: 
                        Console.WriteLine("Opcion no disponible");
                        break;
                }

            }
            while (opcion != 3);
        }
    }
}
